package de.medianox.sbh.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value=HttpStatus.NOT_FOUND, reason="Game Not Found") //404
public class GameNotFoundException extends RuntimeException {

	private static final long serialVersionUID = -3332292346834265371L;

	public GameNotFoundException(long id){
        super("GameNotFoundException with id="+id);
	}
}
