package de.medianox.sbh.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value=HttpStatus.NOT_FOUND, reason="Employee Not Found") //404
public class LetterNotFoundException extends RuntimeException {

	private static final long serialVersionUID = -3332292346834265371L;

	public LetterNotFoundException(String value){
        super("GameNotFoundException with value="+value);
	}
}
