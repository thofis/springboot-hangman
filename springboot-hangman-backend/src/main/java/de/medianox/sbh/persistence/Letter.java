package de.medianox.sbh.persistence;

import de.medianox.sbh.LetterStatus;

import javax.persistence.*;

/**
 * Created by tom on 1/1/17.
 */
@Entity
public class Letter {

    @Id
    @GeneratedValue
    private long id;

    private String value;

    @Enumerated(EnumType.STRING)
    private LetterStatus status;

    @ManyToOne
    private Game game;

    public Letter() {
        status = LetterStatus.UNUSED;
    }

    public Letter(String value) {
        this();
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public LetterStatus getStatus() {
        return status;
    }

    public void setStatus(LetterStatus status) {
        this.status = status;
    }

    public void setGame(Game game) {
        this.game = game;
    }

    @Override
    public String toString() {
        return "Letter{" +
                "id=" + id +
                ", value='" + value + '\'' +
                ", status=" + status +
                '}';
    }

}
