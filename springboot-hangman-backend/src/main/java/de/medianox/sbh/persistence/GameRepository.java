package de.medianox.sbh.persistence;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by tom on 1/1/17.
 */
public interface GameRepository extends JpaRepository<Game, Long> {
}
