package de.medianox.sbh.persistence;

import de.medianox.sbh.LetterStatus;

import javax.persistence.*;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Stream;

/**
 * Created by tom on 1/1/17.
 */
@Entity
public class Game {

    public static final int MAX_MISSES = 7;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    private String phrase;

    @OneToMany(mappedBy = "game", cascade = CascadeType.ALL)
    private Set<Letter> letters;

    public Game(String phrase) {
        this();
        this.phrase = phrase.toUpperCase();
    }

    public Game() {
        letters = new HashSet<>();
    }

    public long getId() {
        return id;
    }

    public Set<Letter> getLetters() {
        return letters;
    }

    public String getPhrase() {
        return isGameOver() ? phrase : getHiddenPhrase();
    }

    /**
     * meant to be only be used by tests!
     * @param phrase the phrase for this game
     */
    public void setPhrase(String phrase) {
        this.phrase = phrase;
    }

    // calculated value form phrase and letterStates
    private String getHiddenPhrase() {
        final Stream<String> stringStream = Arrays.stream(phrase.split("")).map(
                (ch) -> (getLetterStatus(ch) == LetterStatus.SUCCEEDED || !Character.isLetter(ch.charAt(0))) ? ch : "*"
        );
        return stringStream.reduce("", (acc, n) -> acc + n);
    }

    public boolean isGameLost() {
        return getMisses() >= MAX_MISSES;
    }

    public boolean isGameWon() {
        return phrase.equals(getHiddenPhrase());
    }

    public boolean isGameOver() {
        return isGameLost() || isGameWon();
    }

    private LetterStatus getLetterStatus(String ch) {
        return getLetter(letters, ch).getStatus();
    }



    public static Letter getLetter(Set<Letter> letters, String ch) {
        return letters.stream().filter(ltr -> ltr.getValue().equals(ch)).findFirst().orElse(new Letter("|"));
    }



    public void addLetter(String value) {
        final Letter ltr = new Letter(value);
        ltr.setGame(this);
        letters.add(ltr);
    }

    public void updateLetterStatus(String value, LetterStatus status) {
        final Letter letter = getLetter(letters, value);
        letter.setStatus(status);
    }

    public int getMisses() {
        return (int)getLetters().stream().filter(ltr->ltr.getStatus() == LetterStatus.FAILED).count();
    }

    @Override
    public String toString() {
        return "Game{" +
                "id=" + id +
                ", phrase='" + phrase + '\'' +
                '}';
    }

    public boolean containsLetter(String letter) {
        return phrase.contains(letter);
    }
}
