package de.medianox.sbh;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import de.medianox.sbh.persistence.Game;
import de.medianox.sbh.persistence.Letter;
import org.springframework.boot.jackson.JsonComponent;

import java.io.IOException;

/**
 * Created by tom on 1/2/17.
 */
@JsonComponent
public class JsonSerializerForGame extends JsonSerializer<Game> {
    @Override
    public void serialize(Game game, JsonGenerator gen, SerializerProvider serializerProvider) throws IOException, JsonProcessingException {

        gen.writeStartObject();

        gen.writeStringField("phrase", game.getPhrase());
        gen.writeNumberField("misses", game.getMisses());
        gen.writeBooleanField("gameWon", game.isGameWon());
        gen.writeBooleanField("gameLost", game.isGameLost());
        gen.writeBooleanField("gameOver", game.isGameOver());
        gen.writeObjectFieldStart("letters");
        game.getLetters().forEach( (Letter letter) -> {
            try {
                gen.writeStringField(letter.getValue(), letter.getStatus().name());
            } catch (IOException e) {
                throw new IllegalStateException("Couldn't serialize letter "+letter.getValue());
            }
        });
        gen.writeEndObject();

        gen.writeEndObject();
    }
}
