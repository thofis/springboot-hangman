package de.medianox.sbh;

import org.springframework.stereotype.Service;

/**
 * Created by tom on 12/29/16.
 */
@Service
public class DummyPhraseProvider implements PhraseProvider {

    public static final String DEFAULT_PHRASE = "Don't cheat!";

    private final String phrase;

    public DummyPhraseProvider() {
        this.phrase = DEFAULT_PHRASE;
    }

    public DummyPhraseProvider(String phrase) {
        this.phrase = phrase;
    }

    @Override
    public String getPhrase() {
        return phrase;
    }
}
