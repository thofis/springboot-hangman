package de.medianox.sbh;

import de.medianox.sbh.exceptions.GameNotFoundException;
import de.medianox.sbh.persistence.Game;
import de.medianox.sbh.persistence.GameRepository;
import de.medianox.sbh.persistence.Letter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;

/**
 * Created by tom on 1/1/17.
 */
@Service
public class GameService {

    private static final Logger log = LoggerFactory.getLogger(GameService.class);

    private final String ALPHABET = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

    @Autowired
    private GameRepository gameRepository;

    @Autowired
    private PhraseProvider phraseProvider;

    public String createNewGame() {

        final String phrase = phraseProvider.getPhrase();
        if (log.isDebugEnabled()) {
            log.debug("Creating new game with phrase: "+phrase);
        }
        final Game newGame = new Game(phrase);
        Arrays.stream(ALPHABET.split("")).forEach( value -> {
            newGame.addLetter(value);

        } );
        Game savedGame = gameRepository.save(newGame);
        return Long.toString(savedGame.getId());
    }

    public Game getGame(Long id) {
        Game game = gameRepository.getOne(id);
        return game;
    }

    public void guess(long id, String letter) {

        Game game = null;
        try {
            game = gameRepository.findOne(id);

        } catch (IllegalArgumentException e) {
            throw new GameNotFoundException(id);
        }

        Letter ltr = Game.getLetter(game.getLetters(), letter);

        if (game.containsLetter(letter)) {
            ltr.setStatus(LetterStatus.SUCCEEDED);
        } else {
            ltr.setStatus(LetterStatus.FAILED);
        }

        gameRepository.save(game);

    }
}
