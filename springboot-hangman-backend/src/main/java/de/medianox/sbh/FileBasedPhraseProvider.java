package de.medianox.sbh;

import com.google.common.base.Charsets;
import com.google.common.io.Resources;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.net.URL;
import java.util.Random;

/**
 * Created by tom on 1/4/17.
 */
@Service
@Primary
public class FileBasedPhraseProvider implements PhraseProvider {

    private String[] phrases;
    private Random random;

    @PostConstruct
    public void init() {
        URL url = Resources.getResource("phrases.txt");
        try {
            String phrasesString = Resources.toString(url, Charsets.UTF_8);
            phrases = phrasesString.split("\\r?\\n");
        } catch (IOException e) {
            throw new IllegalStateException("Couldn't parse phrases");
        }
        random = new Random();
    }


    @Override
    public String getPhrase() {
        int randomPosition = random.nextInt(phrases.length);
        return phrases[randomPosition];
    }
}
