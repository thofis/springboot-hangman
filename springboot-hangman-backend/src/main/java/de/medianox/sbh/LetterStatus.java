package de.medianox.sbh;

/**
 * Created by tom on 26.12.16.
 */
public enum LetterStatus {
    UNUSED,
    SUCCEEDED,
    FAILED
}
