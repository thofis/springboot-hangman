package de.medianox.sbh;

import de.medianox.sbh.persistence.Game;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import javax.servlet.http.HttpServletRequest;
import java.net.URI;

/**
 * Created by tom on 1/1/17.
 */
@RestController
@RequestMapping("/game")
@CrossOrigin(origins = "http://localhost:8085", exposedHeaders = "location" )
public class GameController {

    @Autowired
    private GameService gameService;

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<?> newGame(HttpServletRequest request) {

        final String gameId = gameService.createNewGame();

        return ResponseEntity.created(createLocationUri(request, gameId)).build();
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<Game> getGame(@PathVariable("id") Long id) {

        Game game = gameService.getGame(id);

        return ResponseEntity.ok(game);
    }

    @RequestMapping(value ="/{id}/guess/{letter}", method = RequestMethod.POST)
    public ResponseEntity<Void> guess(@PathVariable("id") long id, @PathVariable("letter") String letter) {

        gameService.guess(id, letter);

        return ResponseEntity.ok(null);
    }

    private URI createLocationUri(HttpServletRequest request, String gameId) {
        final URI location = UriComponentsBuilder.fromUriString("{baseUrl}/{id}").buildAndExpand(request.getRequestURL(), gameId).toUri();
        return location;
    }
}
