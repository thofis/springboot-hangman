package de.medianox.sbh;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import java.net.URI;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.RANDOM_PORT)
public class ApiIntegrationTest {
    
    @Autowired
    private TestRestTemplate restTemplate;

    //@MockBean
    //GameService gameService;

    private URI gameUri = null;

    @Before
    public void setupNewGame() {
        gameUri = this.restTemplate.postForLocation("/game", null);
    }

    @Test
    public void testGetById() {
        System.out.println(gameUri);
        final ResponseEntity<String> responseEntity = this.restTemplate.getForEntity(
                gameUri.toString(), String.class);

        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
        final String body = responseEntity.getBody();
        assertThat(body).isNotEmpty();
        System.out.println(body);
    }

//    @Test
//    public void mockedGameService() {
//        Game mockGame = new Game();
//        mockGame.setPhrase("Mockito");
//        mockGame.addLetter("A");
//        mockGame.addLetter("B");
//        mockGame.updateLetterStatus("B", LetterStatus.SUCCEEDED);
//        final long gameId = 2L;
//        BDDMockito.given(this.gameService.getGame(gameId)).willReturn(mockGame);
//
//        Map<String, Long> templateVars= new HashMap<>();
//        templateVars.put("id", gameId);
//        final String body = restTemplate.getForObject("/game/{id}", String.class, templateVars);
//
//        System.out.println(body);
//
//        String phrase = JsonPath.read(body, "$.phrase");
//        assertThat(phrase).isEqualTo("Mockito".replaceAll(".", "*"));
//
//        Boolean gameWon = JsonPath.read(body, "$.gameWon");
//        assertThat(gameWon).isFalse();
//
//        final String statusA = JsonPath.read(body, "$.letters.A");
//        assertThat(statusA).isEqualTo("UNUSED");
//        final String statusB = JsonPath.read(body, "$.letters.B");
//        assertThat(statusB).isEqualTo("SUCCEEDED");
//    }

}