# Spring Boot Hangman #

The intention of this project is to showcase different architectural approaches for building a web application. 
For demonstration purposes a simple variant of the hangman game will be built. Such a game wouldn't justify some of the architectural concepts that will be highlighted.

Each architectural approach will be implemented on a separate git branch.
For now the following variants are planned:

* Monolithic application with serverside rendering
* "Microservice approach" (separate components for ui and backend logic) with serverside rendering
* "Microservice approach" with a SinglePageApplication UI (vue.js 2) 



### Building and running the application ###

```
mvn clean package

java -jar target/springboot-hangman-monolith.jar
```
