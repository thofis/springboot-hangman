package de.medianox.sbh.ssf;

/**
 * Created by tom on 26.12.16.
 */
public enum LetterStatus {
    UNUSED,
    SUCCEEDED,
    FAILED
}
