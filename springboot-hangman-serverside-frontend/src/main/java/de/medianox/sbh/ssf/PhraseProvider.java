package de.medianox.sbh.ssf;

/**
 * Created by tom on 12/29/16.
 */
public interface PhraseProvider {
    String getPhrase();
}
