package de.medianox.sbh.ssf;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by tom on 26.12.16.
 */
@Component
@Scope(value="session", proxyMode = ScopedProxyMode.TARGET_CLASS)
public class GameStateSpringSession implements GameState {

    @Autowired
    PhraseProvider phraseProvider;

    public static final int MAX_MISSES = 7;

    public static final String ALPHABET = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

    private Map<String, LetterStatus> letters;

    private String phrase;

    GameStateSpringSession() {
        reset();
    }

    @Override
    public String getPhrase() {
        return gameOver() ? phrase : getHiddenPhrase();
    }

    private String getHiddenPhrase() {
        String newHiddenPhrase = phrase.chars().map( ltr -> isLetterInPhrase((char)ltr) ? ltr : '*' )
                .collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append)
                .toString();
        return newHiddenPhrase;
    }

    private boolean isLetterInPhrase(char letter) {
        return letters.get(Character.toString(letter)) == LetterStatus.SUCCEEDED || !Character.isAlphabetic(letter);
    }

    @Override
    public Map<String, LetterStatus> getLetters() {
        return letters;
    }

    @Override
    public void guessLetter(String letter) {
        if (!letters.containsKey(letter)) {
            throw new IllegalArgumentException("Invalid Letter: "+letter);
        }
        String phrase = getPhrase();
        if (phrase.contains(letter)) {
            letters.put(letter, LetterStatus.SUCCEEDED);
        } else {
            letters.put(letter, LetterStatus.FAILED);
        }
    }

    @Override
    public void reset() {
        letters = new HashMap<>();
        Arrays.stream(ALPHABET.split(""))
                .forEach((letter)-> letters.put(letter, LetterStatus.UNUSED));
        phrase = phraseProvider.getPhrase().toUpperCase();
    }

    @Override
    public int getMisses() {
        return (int)letters.entrySet().stream().filter( entry -> entry.getValue() == LetterStatus.FAILED ).count();
    }

    @Override
    public boolean gameWon() {
        return getPhrase().toUpperCase().equals(getHiddenPhrase());
    }

    @Override
    public boolean gameLost() {
        return getMisses() == MAX_MISSES;
    }

    @Override
    public boolean gameOver() {
        return gameWon() || gameLost();
    }


}
