package de.medianox.sbh.ssf;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Created by tom on 24.12.16.
 */
@Controller
public class HangmanController {


    Logger log = LoggerFactory.getLogger(HangmanController.class);

    @Autowired
    GameState gameState;

    @RequestMapping(value="/hangman", method = RequestMethod.GET)
    public String index(Model model) {
        if (gameState.gameOver()) {
            gameState.reset();
        }
        updateModel(model);
        return "hangman";
    }

    @RequestMapping(value="hangman", method = RequestMethod.POST)
    public String newGame(Model model) {
        gameState.reset();
        updateModel(model);
        return "hangman";
    }

    @RequestMapping("/guess/{letter}")
    public String guess(@PathVariable("letter") String letter, Model model) {
        log.debug("Letter +"+letter);

        gameState.guessLetter(letter);

        updateModel(model);
        return "hangman";
    }

    private void updateModel(Model model) {
        model.addAttribute("phrase", gameState.getPhrase());
        model.addAttribute("letters", gameState.getLetters());
        model.addAttribute("misses", gameState.getMisses());
        model.addAttribute("gameOver", gameState.gameOver());
        model.addAttribute("gameWon", gameState.gameWon());
        model.addAttribute("gameLost", gameState.gameLost());
    }



}
