package de.medianox.sbh.ssf;

import java.util.Map;

/**
 * Created by tom on 26.12.16.
 */

public interface GameState {

    public String getPhrase();

    public Map<String, LetterStatus> getLetters();

    public int getMisses();

    public boolean gameWon();

    public boolean gameLost();

    public boolean gameOver();

    public void guessLetter(String letter);

    public void reset();
}
