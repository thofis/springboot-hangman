package de.medianox.sbh.ssf;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import static com.jayway.jsonpath.JsonPath.read;

/**
 * Created by tom on 1/3/17.
 */
@Component
@Primary
@Scope(value="session", proxyMode = ScopedProxyMode.TARGET_CLASS)
public class GameStateRestClient implements GameState {

    private static final Logger log = LoggerFactory.getLogger(GameStateRestClient.class);

    private RestTemplate restTemplate;

    @Value("${backend.baseUrl}")
    private String baseUrl;

    private URI currentGameUrl = null;

    private String gameJson;

    @Autowired
    public GameStateRestClient(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    @Override
    public String getPhrase() {
        return read(gameJson, "$.phrase");
    }

    @Override
    public Map<String, LetterStatus> getLetters() {
        final String alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        Map<String, LetterStatus> letters = new HashMap<>();
        Arrays.stream(alphabet.split("")).forEach( ltr -> {
            final String statusString = read(gameJson, "$.letters." + ltr);
            letters.put(ltr, LetterStatus.valueOf(statusString));
        });
        return letters;
    }

    @Override
    public int getMisses() {
        return read(gameJson, "$.misses");
    }

    @Override
    public boolean gameWon() {
        return read(gameJson, "$.gameWon");
    }

    @Override
    public boolean gameLost() {
        return read(gameJson, "$.gameLost");
    }

    @Override
    public boolean gameOver() {
        return read(gameJson != null ? gameJson : getGameJson(), "$.gameOver");
    }

    @Override
    public void guessLetter(String letter) {
        String urlTemplate = "{gameUri}/guess/{letter}";
        Map<String, String> urlVariables = new HashMap<>();
        urlVariables.put("gameUri", currentGameUrl.toString());
        urlVariables.put("letter", letter);

        final ResponseEntity<Void> responseEntity = this.restTemplate.postForEntity(urlTemplate, null, Void.class, urlVariables);
        if (responseEntity.getStatusCode().is4xxClientError() || responseEntity.getStatusCode().is5xxServerError()) {
            throw new IllegalStateException("Error during backend communication");
        }
        refreshGameJson();
    }

    @Override
    public void reset() {
        // trigger creation of a new game
        System.out.println("url="+baseUrl);
        Map<String, String> urlVariables = new HashMap<>();
        urlVariables.put("url", baseUrl);
        currentGameUrl = this.restTemplate.postForLocation("{url}/game", null, urlVariables);
        refreshGameJson();
    }

    private void refreshGameJson() {
        gameJson = getGameJson();
    }

    private String getGameJson() {
        if (currentGameUrl == null) {
            log.debug("No running game in this session -> creating a new one");
            reset();
        }
        final String gameJson = this.restTemplate.getForObject(currentGameUrl, String.class);
        if (log.isDebugEnabled()) {
            System.out.println("Backend response for game-resource at : "+currentGameUrl);
            System.out.println(gameJson);
        }
        return gameJson;
    }
}
