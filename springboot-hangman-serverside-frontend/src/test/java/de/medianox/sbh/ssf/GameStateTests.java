package de.medianox.sbh.ssf;

import org.junit.Before;
import org.junit.Test;

import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Created by tom on 12/28/16.
 */
public class GameStateTests {

    private GameState state;

    @Before
    public void setup() {
        state = new GameStateSpringSession();
    }

    @Test
    public void getLetters() {
        final Map<String, LetterStatus> letters = state.getLetters();

        assertNotNull(letters);
        assertEquals(LetterStatus.UNUSED, letters.get("A"));
        assertEquals(LetterStatus.UNUSED, letters.get("Z"));
        assertEquals(GameStateSpringSession.ALPHABET.length(), letters.entrySet().size());
    }

}
